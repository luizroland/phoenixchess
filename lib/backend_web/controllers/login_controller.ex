defmodule BackendWeb.LoginController do
  use BackendWeb, :controller

  def login(conn, %{"email" => email, "password" => password}) do
    user = Backend.Repo.get_by(Backend.User, email: email)

    if user == nil do
      BackendWeb.Util.unauthorized(conn)
    end

    valid_login = Bcrypt.verify_pass(password, user.password_hash)

    if !valid_login do
      BackendWeb.Util.unauthorized(conn)
    end

    payload = %{"id" => user.id}
    token = BackendWeb.Plugs.JwtPlug.generate_token(payload)
    conn = put_resp_cookie(conn, "token", token, http_only: true)
    conn = fetch_cookies(conn)

    case conn.cookies do
      %{"back_url" => back_url} ->
        delete_resp_cookie(conn, "back_url") |> redirect(to: back_url) |> halt

      _ ->
        BackendWeb.Util.success(conn)
    end
  end
end
