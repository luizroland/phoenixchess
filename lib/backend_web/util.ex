defmodule BackendWeb.Util do
  def traverse_changeset_errors(changeset) do
    Ecto.Changeset.traverse_errors(
      changeset,
      fn {msg, opts} ->
        Enum.reduce(opts, msg, fn {key, value}, acc ->
          String.replace(acc, "%{#{key}}", to_string(value))
        end)
      end
    )
  end

  def bad_request_and_errors(conn, changeset) do
    Plug.Conn.put_status(conn, 400)
    |> Phoenix.Controller.json(traverse_changeset_errors(changeset))
  end

  def bad_request(conn) do
    Plug.Conn.put_status(conn, 400)
    |> Phoenix.Controller.json("Bad Request")
  end

  def unauthorized(conn) do
    Plug.Conn.put_status(conn, 401)
    |> Plug.Conn.put_resp_header("www-authenticate", "JWT-Token")
    |> Phoenix.Controller.json("Unauthorized")
  end

  def success(conn) do
    Phoenix.Controller.json(conn, "Success")
  end
end
