defmodule Chess.Board do
  import Chess.Game

  @starting_pieces %{
    :a2 => {:pawn, :white},
    :b2 => {:pawn, :white},
    :c2 => {:pawn, :white},
    :d2 => {:pawn, :white},
    :e2 => {:pawn, :white},
    :f2 => {:pawn, :white},
    :g2 => {:pawn, :white},
    :h2 => {:pawn, :white},
    :a1 => {:rook, :white},
    :b1 => {:knight, :white},
    :c1 => {:bishop, :white},
    :d1 => {:queen, :white},
    :e1 => {:king, :white},
    :f1 => {:bishop, :white},
    :g1 => {:knight, :white},
    :h1 => {:rook, :white},
    :a7 => {:pawn, :black},
    :b7 => {:pawn, :black},
    :c7 => {:pawn, :black},
    :d7 => {:pawn, :black},
    :e7 => {:pawn, :black},
    :f7 => {:pawn, :black},
    :g7 => {:pawn, :black},
    :h7 => {:pawn, :black},
    :a8 => {:rook, :black},
    :b8 => {:knight, :black},
    :c8 => {:bishop, :black},
    :d8 => {:queen, :black},
    :e8 => {:king, :black},
    :f8 => {:bishop, :black},
    :g8 => {:knight, :black},
    :h8 => {:rook, :black}
  }

  def create_board() do
    Enum.map(
      letter_coordinates(),
      fn letter -> Enum.map(number_coordinates(), &(letter <> &1)) end
    )
    |> Enum.reduce([], fn coordinates_list, acc -> acc ++ coordinates_list end)
    |> Map.new(fn coordinate ->
      coordinate = String.to_atom(coordinate)

      case Map.get(@starting_pieces, coordinate, nil) do
        {piece, team} -> {coordinate, Chess.Piece.make_piece(piece, team)}
        _ -> {coordinate, nil}
      end
    end)
  end

  def is_coordinate_valid(board, coordinate) do
    Map.has_key?(board, coordinate)
  end
end
