defmodule Backend.User do
  use Ecto.Schema
  import Ecto.Changeset

  @email_regex ~r<[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?>

  schema "users" do
    field :name, :string
    field :email, :string
    field :email_confirmation, :string, virtual: true
    field :password_hash, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, params) do
    user
    |> cast(params, [:name, :email, :email_confirmation, :password, :password_confirmation])
    |> validate_required([:name, :email, :email_confirmation, :password, :password_confirmation])
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email, name: :user_email_unique)
    |> validate_confirmation(:email)
    |> validate_confirmation(:password)
  end
end
