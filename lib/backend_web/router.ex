defmodule BackendWeb.Router do
  use BackendWeb, :router

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug BackendWeb.Plugs.JwtPlug
  end

  pipeline :api_public do
    plug :accepts, ["json"]
  end

  scope "/", BackendWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/chess", PageController, :chess
  end

  scope "/api", BackendWeb do
    pipe_through :api_public

    resources "/user", UserController, only: [:create]
    post "/login", LoginController, :login
  end

  scope "/api", BackendWeb do
    pipe_through :api

    post "/", ApiController, :test
    get "/", ApiController, :test
  end

  # scope "/graphql", BackendWeb do
  # post "/", Absinthe, schema: BackendWeb.Schema
  # end
end
