defmodule Chess.Game do
  @letter_coordinates [ "a", "b", "c", "d", "e", "f", "g", "h" ]
  @number_coordinates [ "1", "2", "3", "4", "5", "6", "7", "8" ]

  def letter_coordinates, do: @letter_coordinates
  def number_coordinates, do: @number_coordinates
end
