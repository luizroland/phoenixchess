defmodule Chess.Piece do
  import Chess.Game

  @axis_limits %{
    :x => %{
      :upper_limit => "h",
      :bottom_limit => "a"
    },
    :y => %{
      :upper_limit => "8",
      :bottom_limit => "1"
    }
  }

  @pieces_move_atoms %{
    :diagonal_top_left => {-1, 1},
    :diagonal_top_right => {1, 1},
    :diagonal_bot_left => {-1, -1},
    :diagonal_bot_right => {1, -1},
    :vertical_top => {0, 1},
    :vertical_bot => {0, -1},
    :horizontal_left => {-1, 0},
    :horizontal_right => {1, 0}
  }

  @pieces_definition %{
    :pawn => %{
      :type => :pawn,
      :range => :sequence,
      :capture_at => :sequence,
      :can_jump_over => false,
      :has_moved => false,
      :can_sacrifice => true,
      :move_pattern => %{
        :white => [
          [:vertical_top]
        ],
        :black => [
          [:vertical_bot]
        ]
      },
      :capture_on => %{
        :white => [
          [:diagonal_top_left],
          [:diagonal_top_right]
        ],
        :black => [
          [:diagonal_bot_left],
          [:diagonal_bot_right]
        ]
      },
      :special_move => %{
        :pawn_first_move => %{
          :white => [
            [:vertical_top, :vertical_top]
          ],
          :black => [
            [:vertical_bot, :vertical_bot]
          ]
        },
        :pawn_promotion => [:something]
      }
    },
    :rook => %{
      :type => :rook,
      :range => :axis,
      :capture_at => :axis,
      :can_jump_over => false,
      :has_moved => false,
      :can_sacrifice => true,
      :move_pattern => [
        :vertical_top,
        :vertical_bot,
        :horizontal_left,
        :horizontal_right
      ],
      :capture_on => [
        :vertical_top,
        :vertical_bot,
        :horizontal_left,
        :horizontal_right
      ],
      :special_move => %{
        :short_castle => [
          :horizontal_left,
          :horizontal_left
        ],
        :long_castle => [
          :horizontal_right,
          :horizontal_right,
          :horizontal_right
        ]
      }
    },
    :knight => %{
      :type => :knight,
      :range => :sequence,
      :capture_at => :sequence,
      :can_jump_over => true,
      :can_sacrifice => true,
      :move_pattern => [
        [:vertical_top, :vertical_top, :horizontal_right],
        [:vertical_top, :vertical_top, :horizontal_left],
        [:vertical_bot, :vertical_bot, :horizontal_right],
        [:vertical_bot, :vertical_bot, :horizontal_left],
        [:horizontal_right, :horizontal_right, :vertical_top],
        [:horizontal_right, :horizontal_right, :vertical_bot],
        [:horizontal_left, :horizontal_left, :vertical_top],
        [:horizontal_left, :horizontal_left, :vertical_bot]
      ]
    },
    :bishop => %{
      :type => :bishop,
      :range => :axis,
      :capture_at => :axis,
      :can_jump_over => false,
      :can_sacrifice => true,
      :move_pattern => [
        :diagonal_top_left,
        :diagonal_top_right,
        :diagonal_bot_left,
        :diagonal_bot_right
      ],
      :capture_on => [
        :diagonal_top_left,
        :diagonal_top_right,
        :diagonal_bot_left,
        :diagonal_bot_right
      ]
    },
    :queen => %{
      :type => :queen,
      :range => :axis,
      :capture_at => :axis,
      :can_jump_over => false,
      :can_sacrifice => true,
      :move_pattern => [
        :diagonal_top_left,
        :diagonal_top_right,
        :diagonal_bot_left,
        :diagonal_bot_right,
        :vertical_top,
        :vertical_bot,
        :horizontal_left,
        :horizontal_right
      ],
      :capture_on => [
        :diagonal_top_left,
        :diagonal_top_right,
        :diagonal_bot_left,
        :diagonal_bot_right,
        :vertical_top,
        :vertical_bot,
        :horizontal_left,
        :horizontal_right
      ]
    },
    :king => %{
      :type => :king,
      :range => :sequence,
      :capture_at => :sequence,
      :can_jump_over => false,
      :has_moved => false,
      :can_sacrifice => false,
      :move_pattern => [
        [:diagonal_top_left],
        [:diagonal_top_right],
        [:diagonal_bot_left],
        [:diagonal_bot_right],
        [:vertical_top],
        [:vertical_bot],
        [:horizontal_left],
        [:horizontal_right]
      ],
      :capture_on => [
        [:diagonal_top_left],
        [:diagonal_top_right],
        [:diagonal_bot_left],
        [:diagonal_bot_right],
        [:vertical_top],
        [:vertical_bot],
        [:horizontal_left],
        [:horizontal_right]
      ],
      :special_move => %{
        :short_castle => [
          :horizontal_right,
          :horizontal_right
        ],
        :long_castle => [
          :horizontal_left,
          :horizontal_left
        ]
      }
    }
  }

  def make_piece(type, team) do
    Map.put(@pieces_definition[type], :team, team)
  end

  def slice_until_piece(axis_coordinates, board, team) do
    first_piece_index_on_axis =
      Enum.find_index(axis_coordinates, fn coordinate ->
        board[coordinate] != nil
      end)

    if first_piece_index_on_axis == nil do
      axis_coordinates
    else
      piece_coordinate = Enum.at(axis_coordinates, first_piece_index_on_axis)
      piece_team = board[piece_coordinate][:team]
      is_same_team = piece_team == team

      Enum.slice(
        axis_coordinates,
        0,
        if is_same_team do
          first_piece_index_on_axis
        else
          first_piece_index_on_axis + 1
        end
      )
    end
  end

  def gather_axis_diagonal_coordinates(
        board,
        coordinate,
        x_delimeter_fn,
        y_delimiter_fn,
        invert_x,
        invert_y
      ) do
    <<x::utf8>> = Atom.to_string(coordinate) |> String.first()
    <<y::utf8>> = Atom.to_string(coordinate) |> String.last()
    team = board[coordinate][:team]

    all_letters =
      if invert_x do
        Enum.reverse(letter_coordinates())
      else
        letter_coordinates()
      end

    all_numbers =
      if invert_y do
        Enum.reverse(number_coordinates())
      else
        number_coordinates()
      end

    letters =
      Enum.filter(
        all_letters,
        fn letter ->
          <<letter_ascii::utf8>> = letter

          if x_delimeter_fn.(letter_ascii, x) do
            true
          else
            false
          end
        end
      )

    numbers =
      Enum.filter(
        all_numbers,
        fn number ->
          <<number_ascii::utf8>> = number

          if y_delimiter_fn.(number_ascii, y) do
            true
          else
            false
          end
        end
      )

    Enum.zip(letters, numbers)
    |> Enum.map(fn {x, y} -> String.to_atom(x <> y) end)
    |> slice_until_piece(board, team)
  end

  def gather_axis_horizontal_coordinates(board, coordinate, x_delimeter_fn, invert_x) do
    <<x::utf8>> = Atom.to_string(coordinate) |> String.first()
    y = Atom.to_string(coordinate) |> String.last()
    team = board[coordinate][:team]

    all_letters =
      if invert_x do
        Enum.reverse(letter_coordinates())
      else
        letter_coordinates()
      end

    letters =
      Enum.filter(
        all_letters,
        fn letter ->
          <<letter_ascii::utf8>> = letter

          if x_delimeter_fn.(letter_ascii, x) do
            true
          else
            false
          end
        end
      )

    Enum.map(letters, fn x ->
      String.to_atom(x <> y)
    end)
    |> slice_until_piece(board, team)
  end

  def gather_axis_vertical_coordinates(board, coordinate, y_delimiter_fn, invert_y) do
    x = Atom.to_string(coordinate) |> String.first()
    <<y::utf8>> = Atom.to_string(coordinate) |> String.last()
    team = board[coordinate][:team]

    all_numbers =
      if invert_y do
        Enum.reverse(number_coordinates())
      else
        number_coordinates()
      end

    numbers =
      Enum.filter(
        all_numbers,
        fn number ->
          <<number_ascii::utf8>> = number

          if y_delimiter_fn.(number_ascii, y) do
            true
          else
            false
          end
        end
      )

    Enum.map(numbers, fn y ->
      String.to_atom(x <> y)
    end)
    |> slice_until_piece(board, team)
  end

  def get_move_axis_list_from_move_atom(board, coordinate, move_atom) do
    case move_atom do
      :diagonal_top_left ->
        # {-1, 1}
        gather_axis_diagonal_coordinates(board, coordinate, &(&1 < &2), &(&1 > &2), true, false)

      :diagonal_top_right ->
        # {1, 1}
        gather_axis_diagonal_coordinates(board, coordinate, &(&1 > &2), &(&1 > &2), false, false)

      :diagonal_bot_left ->
        # {-1, -1}
        gather_axis_diagonal_coordinates(board, coordinate, &(&1 < &2), &(&1 < &2), true, true)

      :diagonal_bot_right ->
        # {1, -1}
        gather_axis_diagonal_coordinates(board, coordinate, &(&1 > &2), &(&1 < &2), false, true)

      :vertical_top ->
        # {0, 1}
        gather_axis_vertical_coordinates(board, coordinate, &(&1 > &2), false)

      :vertical_bot ->
        # {0, -1}
        gather_axis_vertical_coordinates(board, coordinate, &(&1 < &2), true)

      :horizontal_left ->
        # {-1, 0}
        gather_axis_horizontal_coordinates(board, coordinate, &(&1 < &2), true)

      :horizontal_right ->
        # {1, 0}
        gather_axis_horizontal_coordinates(board, coordinate, &(&1 > &2), false)

      _ ->
        []
    end
  end

  def get_next_coordinate(board, coordinate, move_atom) do
    if !Chess.Board.is_coordinate_valid(board, coordinate) do
      nil
    else
      <<x::utf8>> = Atom.to_string(coordinate) |> String.first()
      <<y::utf8>> = Atom.to_string(coordinate) |> String.last()

      case move_atom do
        :diagonal_top_left ->
          # {-1, 1}
          <<x_bot_limit::utf8>> = @axis_limits[:x][:bottom_limit]
          <<y_upp_limit::utf8>> = @axis_limits[:y][:upper_limit]
          new_coord_x = x - 1
          new_coord_y = y + 1

          if new_coord_x >= x_bot_limit &&
               new_coord_y <= y_upp_limit do
            String.to_atom(<<new_coord_x>> <> <<new_coord_y>>)
          else
            nil
          end

        :diagonal_top_right ->
          # {1, 1}
          <<x_upp_limit::utf8>> = @axis_limits[:x][:upper_limit]
          <<y_upp_limit::utf8>> = @axis_limits[:y][:upper_limit]
          new_coord_x = x + 1
          new_coord_y = y + 1

          if new_coord_x <= x_upp_limit &&
               new_coord_y <= y_upp_limit do
            String.to_atom(<<new_coord_x>> <> <<new_coord_y>>)
          else
            nil
          end

        :diagonal_bot_left ->
          # {-1, -1}
          <<x_bot_limit::utf8>> = @axis_limits[:x][:bottom_limit]
          <<y_bot_limit::utf8>> = @axis_limits[:y][:bottom_limit]
          new_coord_x = x - 1
          new_coord_y = y - 1

          if new_coord_x >= x_bot_limit &&
               new_coord_y >= y_bot_limit do
            String.to_atom(<<new_coord_x>> <> <<new_coord_y>>)
          else
            nil
          end

        :diagonal_bot_right ->
          # {1, -1}
          <<x_upp_limit::utf8>> = @axis_limits[:x][:upper_limit]
          <<y_bot_limit::utf8>> = @axis_limits[:y][:bottom_limit]
          new_coord_x = x + 1
          new_coord_y = y - 1

          if new_coord_x <= x_upp_limit &&
               new_coord_y >= y_bot_limit do
            String.to_atom(<<new_coord_x>> <> <<new_coord_y>>)
          else
            nil
          end

        :vertical_top ->
          # {0, 1}
          <<y_upp_limit::utf8>> = @axis_limits[:y][:upper_limit]
          new_coord_y = y + 1

          if new_coord_y <= y_upp_limit do
            String.to_atom(<<x>> <> <<new_coord_y>>)
          else
            nil
          end

        :vertical_bot ->
          # {0, -1}
          <<y_bot_limit::utf8>> = @axis_limits[:y][:bottom_limit]
          new_coord_y = y - 1

          if new_coord_y >= y_bot_limit do
            String.to_atom(<<x>> <> <<new_coord_y>>)
          else
            nil
          end

        :horizontal_left ->
          # {-1, 0}
          <<x_bot_limit::utf8>> = @axis_limits[:x][:bottom_limit]
          new_coord_x = x - 1

          if new_coord_x >= x_bot_limit do
            String.to_atom(<<new_coord_x>> <> <<y>>)
          else
            nil
          end

        :horizontal_right ->
          # {1, 0}
          <<x_upp_limit::utf8>> = @axis_limits[:x][:upper_limit]
          new_coord_x = x + 1

          if new_coord_x <= x_upp_limit do
            String.to_atom(<<new_coord_x>> <> <<y>>)
          else
            nil
          end

        _ ->
          nil
      end
    end
  end

  def have_team_piece_last_coordinate(board, coordinates_list, team) do
    last_coordinate = List.last(coordinates_list)
    piece = board[last_coordinate]

    if piece != nil do
      piece[:team] == team
    else
      false
    end
  end

  def gather_sequence_coordinates(board, coordinate, atoms_list, can_jump_over \\ false) do
    team = board[coordinate][:team]
    Enum.reduce(atoms_list, [], fn atoms, acc ->
      coordinates_list =
        Enum.reduce(atoms, [], fn atom, acc ->
          last_coordinate =
            case acc do
              [] ->
                coordinate

              _ ->
                List.last(acc)
            end
            next_coordinate = get_next_coordinate(board, last_coordinate, atom)
            acc ++ [next_coordinate]
        end)
        is_valid_moveset =
          Enum.find_index(coordinates_list, fn element -> element == nil end) == nil
          && !have_team_piece_last_coordinate(board, coordinates_list, team)
        if is_valid_moveset do
          acc ++ [List.last(coordinates_list)]
        else
          acc
        end
    end)
  end

  def get_valid_move_list(board, coordinate) do
    case board[coordinate] do
      %{
        :range => :axis,
        :can_jump_over => false,
        :move_pattern => move_pattern
      } ->
        Enum.reduce(move_pattern, [], fn move_atom, acc ->
          get_move_axis_list_from_move_atom(board, coordinate, move_atom) ++ acc
        end)

      %{
        :range => :sequence,
        :can_jump_over => false,
        :move_pattern => %{
          :white => move_pattern_white,
          :black => move_pattern_black
        }
      } ->
        case board[coordinate][:team] do
          :white ->
            gather_sequence_coordinates(board, coordinate, move_pattern_white)

          :black ->
            gather_sequence_coordinates(board, coordinate, move_pattern_black)
        end

      %{
        :range => :sequence,
        :can_jump_over => false,
        :move_pattern => move_pattern
      } ->
        gather_sequence_coordinates(board, coordinate, move_pattern)

      %{
        :range => :sequence,
        :can_jump_over => true,
        :move_pattern => move_pattern
      } ->
        gather_sequence_coordinates(board, coordinate, move_pattern, true)

      [] ->
        []
    end
  end

  def make_move(board, coordinate_from, coordinate_to) do
  end
end
