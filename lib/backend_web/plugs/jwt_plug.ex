defmodule BackendWeb.Plugs.JwtPlug do
  import Plug.Conn

  @secret "243ZqJuy9gES4yDK0WVAE7ngMAyU6zrrjz8nVKteOl/nInT4R5jKSQm+EVxP37ph"
  @token_type "JWT"
  @alg "HS256"
  @jwt_header %{alg: @alg, typ: @token_type}
  @authetication_route "/session/new"

  def init(default), do: default

  def encrypt_base64(object) do
    Jason.encode!(object) |> Base.url_encode64(padding: false)
  end

  def encrypt_hmac_base64(object) do
    :crypto.hmac(:sha256, @secret, object) |> Base.url_encode64(padding: false)
  end

  def generate_header_body(data) do
    header_encrypted = encrypt_base64(@jwt_header)
    body_encrypted = encrypt_base64(data)
    header_encrypted <> "." <> body_encrypted
  end

  def generate_token(data) do
    jwt_header_body = generate_header_body(data)
    jwt_header_body <> "." <> encrypt_hmac_base64(jwt_header_body)
  end

  def check_header_match(token) do
    token_parts = String.split(token, ".")

    if length(token_parts) != 3 do
      "Invalid Token"
    end

    header = List.first(token_parts)
    encrypt_base64(@jwt_header) == header
  end

  def check_signature_match(token) do
    token_parts = String.split(token, ".")

    if length(token_parts) != 3 do
      "Invalid Token"
    end

    signature = List.last(token_parts)
    header = token_parts |> List.first()
    body = token_parts |> Enum.at(1)
    encrypt_hmac_base64(header <> "." <> body) == signature
  end

  def token_valid(token) do
    check_header_match(token) &&
      check_signature_match(token) &&
      true
  end

  def req_have_valid_token_cookie(conn) do
    conn = fetch_cookies(conn)

    case conn.cookies do
      %{"token" => token} -> token_valid(token)
      _ -> false
    end
  end

  def call(conn, _default) do
    case req_have_valid_token_cookie(conn) do
      false ->
        conn = put_resp_cookie(conn, "back_url", conn.request_path, http_only: true)
        conn |> Phoenix.Controller.redirect(to: @authetication_route) |> halt

      _ ->
        conn
    end
  end
end
