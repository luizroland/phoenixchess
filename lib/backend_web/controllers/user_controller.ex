defmodule BackendWeb.UserController do
  use BackendWeb, :controller

  def create(conn, param) do
    case param do
      %{
        "name" => name,
        "email" => email,
        "email_confirmation" => email_confirmation,
        "password" => password,
        "password_confirmation" => password_confirmation
      } ->
        parameters = %{
          name: name,
          email: email,
          email_confirmation: email_confirmation,
          password: password,
          password_confirmation: password_confirmation
        }

        changeset = Backend.User.changeset(%Backend.User{}, parameters)

        changeset =
          if changeset.valid? do
            Ecto.Changeset.change(changeset, Bcrypt.add_hash(password))
            |> Ecto.Changeset.change(password_confirmation: nil)
          end

        Backend.Repo.insert(changeset)
        |> case do
          {:ok, record} ->
            json(conn, %{id: record.id})

          {:error, changeset} ->
            BackendWeb.Util.bad_request_and_errors(conn, changeset)
        end

      _ ->
        BackendWeb.Util.bad_request(conn)
    end
  end
end
